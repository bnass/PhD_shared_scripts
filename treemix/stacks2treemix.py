# stacks2treemix.py v0.3.1

# This script eliminates comments and lines containing '0,0' (as their presence may cause errors)
# from a VCF file generated during the population post-processing for TreeMix conversion and gzips
# the output. Input will be overwritten, please create backups prior.

# Usage: python3 stacks2treemix.py <INPUT> [--remove-comments]

import argparse
import gzip
import os
import re

def remove_lines_with_pattern(file_path, pattern, remove_comments):
    # Read the content of the file
    with open(file_path, 'r') as file:
        content = file.read()

    # Compile regex pattern to match '0,0' or '\n0,0'
    regex_pattern = re.compile(r'^.*{}.*$'.format(pattern), re.MULTILINE)

    # Remove lines matching the regex pattern
    content = re.sub(regex_pattern, '', content)

    # Optionally, remove lines starting with #
    if remove_comments:
        content = re.sub(r'^#.*\n?', '', content, flags=re.MULTILINE)

    # Remove empty lines
    content = re.sub(r'^\n', '', content, flags=re.MULTILINE)

    # Write the modified content to a temporary file
    temp_file_path = file_path + '.tmp'
    with open(temp_file_path, 'w') as temp_file:
        temp_file.write(content)

    # Compress the temporary file using gzip
    with open(temp_file_path, 'rb') as temp_file:
        with gzip.open(file_path + '.gz', 'wb') as gzipped_file:
            gzipped_file.writelines(temp_file)

    # Remove the temporary file
    os.remove(temp_file_path)

def main():
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser(description='Remove lines containing a specific pattern from a text file and gzip the output.')

    # Add the input file argument
    parser.add_argument('input_file', help='Path to the input text file')

    # Add an optional argument to remove lines starting with #
    parser.add_argument('--remove-comments', action='store_true', help='Remove lines starting with #')

    # Parse the command-line arguments
    args = parser.parse_args()

    # Specify the pattern to remove, including the new pattern to match lines ending with '0,0'
    pattern_to_remove = r'( 0,0|\n0,0)'

    # Remove lines containing the specified pattern and optionally lines starting with #
    remove_lines_with_pattern(args.input_file, pattern_to_remove, args.remove_comments)

if __name__ == "__main__":
    main()
