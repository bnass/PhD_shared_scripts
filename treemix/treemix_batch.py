# treemix_batch.py v0.3

# Automates the execution of Treemix commands for a specified input file and a range of values.
# Requires TreeMix v. 1.13 (bitbucket.org/nygcresearch/treemix) to be installed and on path.

# Ensure that you have the necessary permissions to execute the script and write files in the
# output directory. Please note that prior knowledge of Treemix and its command-line options is
# assumed. Make sure you understand Treemix and its parameters to use this script effectively.

# DEFAULT (hard-coded) Treemix command of this script is:
# treemix -i <INPUT> -m <EDGE> -o <OUTPUT> -r <ROOT> -bootstrap -k 1000 -noss > <LOG>"

# usage: python3 treemix_batch.py <input_file> -e <START END> [-o OUTPUT]

import argparse
import subprocess
import os

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description='Process Treemix command with input file and edges.')

# Add the required argument for the full input file name
parser.add_argument('input_file', type=str, help='full input file name including extension')

# Add the required argument for the migration edges (start and end values)
parser.add_argument('-e', '--edges', type=int, nargs=2, metavar=('start', 'end'), help='range of migration edge values (start end)')

# Add the optional argument for the output file name
parser.add_argument('-o', '--output', type=str, help='output file name prefix')

# Parse the command-line arguments
args = parser.parse_args()

# Validate the edges values
if not args.edges:
    parser.error('Please specify the edges (start and end values) using the -e/--edges option.')

start_range, end_range = args.edges
if start_range < 0 or end_range < 0 or end_range < start_range:
    parser.error('Invalid edges values. The start value should be less than or equal to the end value.')

# Extract the input file name
input_file = args.input_file

# Check if the input file exists
if not os.path.isfile(input_file):
    parser.error(f"The input file '{input_file}' does not exist.")

# Set the output file name prefix
if args.output:
    output_prefix = args.output
else:
    output_prefix = os.path.splitext(input_file)[0]  # Use input file name as prefix

# Create the output directory if it doesn't exist
output_directory = 'treemix_output'
os.makedirs(output_directory, exist_ok=True)

# Execute the Treemix command for the specified range
for i in range(start_range, end_range + 1):
    # Construct the command
    output_file = os.path.join(output_directory, f"{output_prefix}.{i}")
    command = f"treemix -i {input_file} -m {i} -o {output_file} -root change_to_actual_root -bootstrap -k 1000 -noss > treemix_{i}.log"

    # Execute the command using subprocess
    subprocess.Popen(command, shell=True)