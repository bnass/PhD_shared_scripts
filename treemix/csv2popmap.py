# csv2popmap.py v0.2

# Extracts chosen rows from a .csv, converts them to .txt and
# removes the header. Used to create popmap files for stacks.

# usage: python3 csv2popmap.py <input> <columnX> <columnY> <...>

import csv
import argparse

def extract_and_convert(input_file, output_file, columns):
    try:
        with open(input_file, 'r', newline='') as file:
            reader = csv.DictReader(file)
            headers = reader.fieldnames
            selected_columns = [header for header in headers if header in columns]

            if not selected_columns:
                print("Error: No matching columns found in the input file.")
                return

            with open(output_file, 'w', newline='') as output:
                writer = csv.DictWriter(output, fieldnames=selected_columns)
                writer.writeheader()

                for row in reader:
                    selected_row = {header: row[header] for header in selected_columns}
                    writer.writerow(selected_row)

            #print("Extraction complete. The selected columns have been saved to:", output_file)

        # Convert extracted CSV to modified TXT
        with open(output_file, 'r') as csv_file:
            lines = csv_file.readlines()
            modified_lines = [line.replace(',', '\t') for line in lines[1:]]  # Modify content

        txt_output_file = output_file.replace('.csv', '.txt')
        with open(txt_output_file, 'w') as txt_file:
            txt_file.writelines(modified_lines)

        print("Conversion complete. The CSV file has been converted and modified to:", txt_output_file)
    except IOError:
        print("Error: Failed to read or write files.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CSV Column Extractor and Converter')
    parser.add_argument('input_file', help='Path to the input CSV file')
    parser.add_argument('columns', nargs='+', help='Column names to extract and convert')
    
    args = parser.parse_args()

    output_csv_file = 'popmap'
    extract_and_convert(args.input_file, output_csv_file, args.columns)
