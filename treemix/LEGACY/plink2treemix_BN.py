# plink2treemix_BN.py v0.3 by Benneth Nass (nassb@fzp.czu.cz)

# This script converts plink clustered allele frequencies to treemix format.
# Based on plink2treemix by Thom Nelson (https://github.com/thomnelson/tools/blob/master/plink2treemix.py)

# usage: python3 plink2treemix_BN.py <plink.frq.strat> <output>

import sys
import time
import pandas as pd

# Measure elapsed time
start_time = time.time()

# Check if the correct number of command-line arguments is provided
if len(sys.argv) != 3:
    sys.exit("\nUsage: plink2treemix_BN.py <plink.frq.strat> <output>\n")

plink_freq_file = sys.argv[1]
output_file = sys.argv[2]

# Read the plink frequency file
try:
    freq_df = pd.read_csv(plink_freq_file, delim_whitespace=True)
except FileNotFoundError:
    sys.exit("Error: Input file not found.")

snp_IDs = freq_df["SNP"].unique()
pop_IDs = freq_df["CLST"].unique()
npops = len(pop_IDs)

# Write population IDs to the output file
with open(output_file, 'w') as f:
    f.write(' '.join(pop_IDs))
    f.write("\n")

nsnps_out = 0
for snp in snp_IDs:
    snp_df = freq_df[freq_df["SNP"] == snp]
    unique_freqs = snp_df["MAC"].unique().tolist()
    # Enforce presence of site in all populations
    if snp_df.shape[0] != npops:
        continue
    nsnps_out += 1
    # Display the progress of SNP writing
    sys.stdout.write("SNPs written: %s\r" % nsnps_out)
    sys.stdout.flush()
    allele_counts_per_pop = []
    for pop in pop_IDs:
        pop_df = snp_df[snp_df["CLST"] == pop]
        # Check if there are records for the population
        if pop_df.empty:
            sys.exit(f"Error: No data found for population '{pop}'.")
        nalleles = pop_df.at[pop_df.index[0], "NCHROBS"]
        minor_count = pop_df.at[pop_df.index[0], "MAC"]
        major_count = nalleles - minor_count
        allele_counts = f"{major_count},{minor_count}"
        allele_counts_per_pop.append(allele_counts)
    allele_counts_str = " ".join(allele_counts_per_pop)
    # Append allele counts to the output file
    with open(output_file, 'a') as f:
        f.write(f"{allele_counts_str}\n")

# Display completion message
sys.stderr.write("Output written to file: %s\n" % output_file)
print(f"plink2treemix_BN.py completed successfully. Elapsed time: {time.time() - start_time:.2f} seconds")