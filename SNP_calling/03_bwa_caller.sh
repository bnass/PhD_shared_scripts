#!/bin/bash
# Version 1.2a

# Currently, only .FASTA extension for the reference is allowed.
# It is recommended to index the reference before using this script.

# Check if arguments are provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <fastq_directory> [<reference_fasta_file>]"
    exit 1
fi

# Specify the directory containing your FASTQ files
fastq_dir="$1"

# Check if the specified directory exists
if [ ! -d "$fastq_dir" ]; then
    echo "Directory not found: $fastq_dir"
    exit 1
fi

# Use the provided reference FASTA file or look in the "reference" directory
if [ -n "$2" ]; then
    ref_fasta="$2"
else
    ref_fasta=$(find "${fastq_dir}/reference" -maxdepth 1 -type f -name "*.fasta" | head -n 1)
fi

# Check if the reference FASTA file exists
if [ ! -e "$ref_fasta" ]; then
    echo "Reference FASTA file not found: $ref_fasta"
    exit 1
else
	echo "Reference FASTA file found: $ref_fasta"
fi

# Check if the reference FASTA file has been indexed; if not, index it
ref_index="${ref_fasta}".bwt
if [ ! -e "$ref_index" ]; then
    echo "Indexing reference FASTA file. Please wait..."
    module load bwa
    bwa index "$ref_fasta"
    if [ $? -eq 0 ]; then
        echo "Reference FASTA file has been indexed."
    else
        echo "Error indexing reference FASTA file."
        exit 1
    fi
fi

cd $fastq_dir

# Create a directory for trimmed read files
mkdir -p trimmed_reads/
# Create a directory for the sorted bams
mkdir -p sorted_bams/
# Create a directory for the samtools flagstats logs
mkdir -p flagstats_logs/

# Loop through each pair of R1 and R2 FASTQ files
for r1_file in "$fastq_dir"/*R1.trimmed.fastq.gz; do
    # Ensure that there is a corresponding R2 file
    r2_file="${r1_file%R1.trimmed.fastq.gz}R2.trimmed.fastq.gz"
    
    if [ ! -e "$r2_file" ]; then
        echo "No corresponding R2 file found for $r1_file. Skipping."
        continue
    fi

    # Sanitize the job name based on the file names
    job_name="$(basename "$r1_file" | tr -cs '[:alnum:]_' '_')"
	sample_name="$(basename ${r1_file%.R1.trimmed.fastq.gz})"

    qsub <<EOT
#!/bin/bash
#PBS -N bwa_mem_sort_${job_name}
#PBS -l select=1:ncpus=12:mem=16gb:scratch_local=50gb
#PBS -l walltime=84:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
echo "Loading required modules..."
module add bwa
module add samtools

echo "Switching path to $fastq_dir"
cd $fastq_dir

# Run BWA MEM and pipe the output to samtools sort
echo "Starting read mapping..."
bwa mem -t 12 $ref_fasta $r1_file $r2_file | samtools sort -T ${r1_file%.R1.trimmed.fastq.gz} -@12 -o ${r1_file%.R1.trimmed.fastq.gz}.sorted.bam -

# Move read files
echo "Moving read files..."
mv $r1_file trimmed_reads/
mv $r2_file trimmed_reads/

# Check and remove secondary/supplementary alignments
echo "Removing secondary/supplementary reads..."
samtools flagstat ${r1_file%.R1.trimmed.fastq.gz}.sorted.bam > ${r1_file%.R1.trimmed.fastq.gz}.pre_filter.flagstat.log
samtools view -b -F 4 -F 256 -F 2048 -o ${r1_file%.R1.trimmed.fastq.gz}.filtered.bam ${r1_file%.R1.trimmed.fastq.gz}.sorted.bam
samtools flagstat ${r1_file%.R1.trimmed.fastq.gz}.filtered.bam > ${r1_file%.R1.trimmed.fastq.gz}.post_filter.flagstat.log

# Move sorted bams
echo "Moving unfiltered bams..."
mv ${r1_file%.R1.trimmed.fastq.gz}.sorted.bam sorted_bams/

# Move flagstat logs
echo "Moving flagstat logs..."
mv ${r1_file%.R1.trimmed.fastq.gz}.pre_filter.flagstat.log flagstats_logs/
mv ${r1_file%.R1.trimmed.fastq.gz}.post_filter.flagstat.log flagstats_logs/

echo "Done! Exiting..."
EOT
    echo "Job for sample ${sample_name} has been submitted!"
done
