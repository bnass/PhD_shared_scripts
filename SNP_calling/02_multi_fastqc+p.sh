#!/bin/bash
# Version 1.1

# added phred cutoff in fastp by <=Q25

# Check if an argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <fastq_directory>"
    exit 1
fi

# Specify the directory containing your paired-end FASTQ files
fastq_dir="$1"

# Check if the specified directory exists
if [ ! -d "$fastq_dir" ]; then
    echo "Directory not found: $fastq_dir"
    exit 1
fi

cd $fastq_dir

# Create necessary directories:
# preprocessed reads
mkdir -p raw_reads/
# fastqc_before_trimming
mkdir -p fastQC_before_trimming/
# fastqc_after_trimming
mkdir -p fastQC_after_trimming/
# fastp reports
mkdir -p fastp_reports/

# Loop through each pair of R1 and R2 FASTQ files
for r1_file in "$fastq_dir"/*R1.fastq.gz; do
    # Ensure that there is a corresponding R2 file
    r2_file="${r1_file%R1.fastq.gz}R2.fastq.gz"
    
    if [ ! -e "$r2_file" ]; then
        echo "No corresponding R2 file found for $r1_file. Skipping."
        continue
    fi

    # Sanitize the job name based on the file names
    job_name="$(basename "$r1_file" | tr -cs '[:alnum:]_' '_')"

    qsub <<EOT
#!/bin/bash
#PBS -N fastq_trimming_${job_name}
#PBS -l select=1:ncpus=8:mem=12gb:scratch_local=10gb
#PBS -l walltime=24:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
echo "Loading required modules..."
module add fastp
module add fastqc
#module add python27-modules-gcc

echo "Switching path to $fastq_dir"
cd $fastq_dir

# Run fastQC for untrimmed reads
echo "Starting first fastQC run"
fastqc -o fastQC_before_trimming/ $r1_file
fastqc -o fastQC_before_trimming/ $r2_file

# Run Fastp on the paired-end reads (trimming)
echo "Running fastp..."
fastp --in1 $r1_file --in2 $r2_file --out1 ${r1_file%fastq.gz}trimmed.fastq.gz --out2 ${r2_file%fastq.gz}trimmed.fastq.gz -l 50 -q 25 -h ${r1_file%R1.fastq.gz}html

# Move untrimmed reads & fastp_reports
echo "Moving untrimmed reads"
mv $r1_file raw_reads/
mv $r2_file raw_reads/
mv *.html fastp_reports/
mv *.json fastp_reports/

# Run fastQC for untrimmed reads
echo "Starting second fastQC run"
fastqc -o fastQC_after_trimming/ ${r1_file%fastq.gz}trimmed.fastq.gz
fastqc -o fastQC_after_trimming/ ${r2_file%fastq.gz}trimmed.fastq.gz

echo "Done! Exiting..."
EOT
    echo "Job for $r1_file and $r2_file has been submitted!"
done
