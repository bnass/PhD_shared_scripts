#!/bin/bash

# 07_MergeGVCFs_bcftools.sh v1.5c

# This script merges all the chromosomal GVCFs created by HaplotypeCaller per subgenome.
# Merging to full genome is also possible, but disabled by default.

# Usage: bash 07_MergeGVCFs_bcftools.sh <gvcf_directory>

# Check if one argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <gvcf_directory>"
    exit 1
fi

# Specify the directory containing the gVCF files and completion log files
gvcf_dir="$1"

# Check if the specified gvcf directory exists
if [ ! -d "$gvcf_dir" ]; then
    echo "Directory not found: $gvcf_dir"
    exit 1
fi

# Change directory to workspace
cd $gvcf_dir

# Discover species names from gVCF filenames (change ".Cs1C.gvcf.gz" to actual file extension)
species_names=($(find "$gvcf_dir" -maxdepth 1 -name '*.Cs1C.gvcf.gz' -exec basename {} \; | cut -d'.' -f1 | sort -u))

# Iterate over each species and concatenate their gVCF files
for species in "${species_names[@]}"; do
    # Flag to indicate if all files are found for the current species
    all_files_found=true

    # Checking for index files is recommended because they are created on completed runs!
	# Check for the presence of all 9 completion log files (C subgenome) for the current species (adjustable)
    for chr in {1..9}C; do
        if [ ! -f "$gvcf_dir/${species}.Cs${chr}.gvcf.gz.tbi" ]; then
            echo "Missing completion log file for ${species} Cs${chr}. Please make sure all logs exist before merging."
            all_files_found=false
        fi
    done

	# Check for the presence of all 9 completion log files (D subgenome) for the current species (adjustable)
    for chr in {1..9}D; do
        if [ ! -f "$gvcf_dir/${species}.Cs${chr}.gvcf.gz.tbi" ]; then
            echo "Missing completion log file for ${species} Cs${chr}. Please make sure all logs exist before merging."
            all_files_found=false
        fi
    done

    if [ "$all_files_found" = true ]; then
        # Sanitize the job name to replace problematic characters
        job_name="$(basename "$species" | tr -cs '[:alnum:]_' '_')"
        
        qsub <<EOT
#!/bin/bash
#PBS -N combining_GVCFs_${job_name}
#PBS -l select=1:ncpus=2:mem=4gb:scratch_local=10gb
#PBS -l walltime=1:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
echo "Loading required modules..."
module add bcftools
module add samtools

echo "Switching path to $gvcf_dir"
cd $gvcf_dir

echo "Begin merging..."
# Concatenate the gVCF files in order for "C" chromosomes
bcftools concat -o ${species}_C.gvcf $(for chr in {1..9}C; do echo -n "$gvcf_dir/${species}.Cs${chr}.gvcf.gz "; done)

# Change species header to reflect the subgenome (disable for full genome merging)
sed -i "s/^#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t.*/#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t${job_name}C/" ${species}_C.gvcf

# Compress the merged gVCF file
bgzip ${species}_C.gvcf

# Index the compressed gVCF file
tabix -p vcf ${species}_C.gvcf.gz

# Concatenate the gVCF files in order for "D" chromosomes
bcftools concat -o ${species}_D.gvcf $(for chr in {1..9}D; do echo -n "$gvcf_dir/${species}.Cs${chr}.gvcf.gz "; done)

# Change species header to reflect the subgenome (disable for full genome merging)
sed -i "s/^#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t.*/#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t${job_name}D/" ${species}_D.gvcf

# Compress the merged gVCF file
bgzip ${species}_D.gvcf

# Index the compressed gVCF file
tabix -p vcf ${species}_D.gvcf.gz

# Concatenate the two results for each species (disabled by default)
#bcftools concat -o ${species}_C+D.gvcf ${species}_C.gvcf.gz ${species}_D.gvcf.gz
#bgzip ${species}_C+D.gvcf
#tabix -p vcf ${species}_C+D.gvcf.gz

echo "Merged and gzipped gVCF files have been created for all species in the directory."
EOT

        echo "Job for merging the GVCFs for ${species} has been submitted!"
    else
        echo "Skipping species ${species} due to missing files."
    fi
done