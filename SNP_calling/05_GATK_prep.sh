#!/bin/bash

# 05_GATK_prep.sh v1.2a

# Modifies bam files for GATK.

# Check if an argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <bam_directory>"
    exit 1
fi

# Set your Read Group options
RGLB="library.lib1"  # Library name
RGPU="1.unit1"       # Run counter
RGPL="Illumina"      # Sequencing platform
#RGSM=Sample		 # Sample name, is set automatically

# Print the Read Group options to the terminal
echo "These are your read groups:"
echo "Read Group Library: $RGLB"
echo "Read Group Platform Unit: $RGPU"
echo "Read Group Platform: $RGPL"
# echo "Read Group Sample: $RGSM"  # Note: Sample name is set automatically by default

# Specify the directory containing your .bam files
bam_dir="$1"

# Check if the specified directory exists
if [ ! -d "$bam_dir" ]; then
    echo "Directory not found: $bam_dir"
    exit 1
fi

cd $bam_dir
mkdir filtered_bams

# Loop through each .bam file and submit a job
for bam_file in "$bam_dir"/*.filtered.bam; do
    # Sanitize the job name to replace problematic characters
    job_name="$(basename "$bam_file" | tr -cs '[:alnum:]_' '_')"
	# Create a temporary directory for this chromosome
    tmp_dir="${job_name}tmp"
    mkdir -p "$tmp_dir"
    
    qsub <<EOT
#!/bin/bash
#PBS -N bam_processing_${job_name}
#PBS -l select=1:ncpus=12:mem=30gb:scratch_local=20gb
#PBS -l walltime=10:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
module add gatk
module add jdk
module add samtools

cd $bam_dir

# Perform GATK operations
gatk --java-options "-Xmx12g" AddOrReplaceReadGroups -I $bam_file -O ${bam_file%.filtered.bam}.rg.bam --RGLB $RGLB --RGPU $RGPU --RGPL $RGPL --RGSM ${bam_file%.filtered.bam}
gatk --java-options "-Xmx12g" MarkDuplicates --INPUT ${bam_file%.filtered.bam}.rg.bam --OUTPUT ${bam_file%.filtered.bam}.md.bam --METRICS_FILE ${bam_file%.filtered.bam}.metrics.txt --TMP_DIR $tmp_dir --MAX_FILE_HANDLES_FOR_READ_ENDS_MAP 1000

# Remove the RG bam (not recommended due to potential errors in GATK)
rm ${bam_file%.filtered.bam}.rg.bam

# Remove the temporary directory
rm -d $tmp_dir

# Index the final bam
samtools index ${bam_file%.filtered.bam}.md.bam

#Move input files (disable if plotting script runs in parallel)
mv $bam_file filtered_bams

EOT

    echo "GATK processing for ${bam_file} has been submitted!"
done
