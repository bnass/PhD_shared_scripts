#!/bin/bash
# Version 1.2

#PBS -N 231007_GenotypeGVCFs
#PBS -l walltime=144:0:0
#PBS -l select=1:ncpus=12:mem=16gb:scratch_local=40gb
#PBS -j oe
#PBS -m abe

module load jdk
module load gatk

# for later: determine all GVCF files
# find . -type f -name "*C+D.gvcf.gz" | sort

cd /storage/brno2/home/nassb/sequencing/Benneth1/GATK/snp_calling/chr_test/subgenomes

#find . -type f -name "*C+D.gvcf.gz" | sort > C+D.list
#find . -type f -name "*C.gvcf.gz" | sort > C.list
#find . -type f -name "*D.gvcf.gz" | sort > D.list

#gatk --java-options "-Xmx16g -XX:ParallelGCThreads=12" CombineGVCFs -R /storage/brno2/home/nassb/sequencing/ref/strictum_all_chrs.fasta -V C+D.list -O combined_tetra_8pop.gvcf.gz
gatk --java-options "-Xmx16g -XX:ParallelGCThreads=12" CombineGVCFs -R /storage/brno2/home/nassb/sequencing/ref/strictum_all_chrs.fasta -V C.list -O C_tetra_8pop.gvcf.gz
#gatk --java-options "-Xmx16g -XX:ParallelGCThreads=12" CombineGVCFs -R /storage/brno2/home/nassb/sequencing/ref/strictum_all_chrs.fasta -V D.list -O D_tetra_8pop.gvcf.gz

gatk --java-options "-Xmx16g -XX:ParallelGCThreads=12" GenotypeGVCFs -R /storage/brno2/home/nassb/sequencing/ref/strictum_all_chrs.fasta -V C_tetra_8pop.gvcf.gz -O C_tetra_8pop.vcf.gz
#gatk --java-options "-Xmx16g -XX:ParallelGCThreads=12" GenotypeGVCFs -R /storage/brno2/home/nassb/sequencing/ref/strictum_all_chrs.fasta -V D_tetra_8pop.gvcf.gz -O D_tetra_8pop.vcf.gz