#!/bin/bash

# 01_fastq_rename.sh v1.2

# Replaces filenames based on a tab-seperated list.

# Get a list of all your FASTQ files with:
# $find . -type f -name "*fastq.gz" | sort > list

# Usage: $ bash 01_fastq_rename.sh <tsv_file> <directory>

# Check if the correct number of arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <tsv_file> <directory>"
    exit 1
fi

tsv_file=$1
directory=$2

# Check if the TSV file exists
if [ ! -e "$tsv_file" ]; then
    echo "Error: TSV file not found - $tsv_file"
    exit 1
fi

# Check if the specified directory exists
if [ ! -d "$directory" ]; then
    echo "Error: Directory not found - $directory"
    exit 1
fi

# Read each line from the TSV file
while IFS=$'\t' read -r filename new_name || [[ -n "$filename" ]]; do
    # Construct current and new paths
    current_path="$directory/$filename"
    new_path="$directory/$new_name"

    # Check if the current file exists
    if [ -e "$current_path" ]; then
        # Attempt to rename the file
        mv -i "$current_path" "$new_path"

        # Check if the renaming was successful
        if [ $? -eq 0 ]; then
            echo "Renamed: $filename -> $new_name"
        else
            echo "Error: Failed to rename $filename"
        fi
    else
        echo "File not found: $filename"
    fi
done < <(tr -d '\r' < "$tsv_file")
