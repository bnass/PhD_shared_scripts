#!/bin/bash

# 04_coverage_visualisation.sh v0.2

# Creates coverage plots out of each bam in a specified directory.
# THIS SCRIPT IS OPTIONAL! It might run in parallel with 05_GATK_prep.sh.

# Please prepare the python scripts like in the Codeberg repository.
# Usage: $ bash 04_coverage_visualisation.sh <bam_directory>

# Check if an argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <bam_directory>"
    exit 1
fi

# Specify the directory containing your .bam files
bam_dir="$1"

# Check if the specified directory exists
if [ ! -d "$bam_dir" ]; then
    echo "Directory not found: $bam_dir"
    exit 1
fi

cd $bam_dir

# Check if "coverage_visualisation" exists
if [ ! -d "coverage_visualisation" ]; then
    echo "Error: Directory 'coverage_visualisation' not found."
    echo "Please create it and put the coverage plot generator scripts inside!"
    exit 1  # Exit with an error code
else
    echo "The folder 'coverage_visualisation' exists."
fi

# Check if the Python scripts exist inside the folder
python_scripts=("coverage_boxplot_generator.py" "coverage_scatterplot_generator.py" "coverage_violinplot_generator.py")

missing_scripts=()
for script in "${python_scripts[@]}"; do
    if [ ! -f "coverage_visualisation/$script" ]; then
        missing_scripts+=("$script")
    fi
done

if [ ${#missing_scripts[@]} -eq 0 ]; then
    echo "All required Python scripts are present."
else
    echo "Warning: Some plotting scripts might be missing:"
    for missing_script in "${missing_scripts[@]}"; do
        echo "  - $missing_script"
    done
fi

echo "Submitting jobs..."

# Loop through each .bam file and submit a job
for bam_file in "$bam_dir"/*.filtered.bam; do
    # Sanitize the job name to replace problematic characters
    job_name="$(basename "$bam_file" | tr -cs '[:alnum:]_' '_')"
    
    qsub <<EOT
#!/bin/bash
#PBS -N ${job_name}coverage_plots
#PBS -l select=1:ncpus=4:mem=50gb:scratch_local=20gb
#PBS -l walltime=10:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
module add samtools

cd $bam_dir

# Perform samtools depths in prep for the coverage plots
samtools depth $bam_file > ${bam_file%.filtered.bam}.log

cd coverage_visualisation/

# Unload modules due to conflicts
module purge
module add python36-modules-gcc

# Execute the plotting scripts
# Disable/replace these scripts however you like (all enabled by default)
python3 coverage_scatterplot_generator.py ${bam_file%.filtered.bam}.log
python3 coverage_boxplot_generator.py ${bam_file%.filtered.bam}.log
python3 coverage_violinplot_generator.py ${bam_file%.filtered.bam}.log

# Moving .log backups
cd $bam_dir
mv ${bam_file%.filtered.bam}.log coverage_visualisation/depth_logs

EOT

    echo "Plotting jobs for ${bam_file} has been submitted!"

	mkdir -p coverage_visualisation/depth_logs
done
