# coverage_scatterplot_generator.py v0.4.3a

# Generate an average coverage per chromosome plot by using the samtools depth output.
# Usage: python3 coverage_scatterplot_generator.py <INPUT>

import argparse
import math
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import numpy as np
import os
import sys
import time

# Argparse block
parser = argparse.ArgumentParser(description="Generate an coverage plot based on the samtools depth output.")
parser.add_argument("input_file", type=str, help="Path to the input file")
args = parser.parse_args()

# Input file handling
file_path = args.input_file
file_base_name, file_extension = os.path.splitext(os.path.basename(file_path))

# Start the timer
start_time = time.time()

# Dictionary to store total sum, count, squared differences, and count of zeros for each identifier
identifier_stats = {}

with open(file_path, "r") as file:

    for line in file:
        columns = line.strip().split("\t")
        identifier = columns[0]  # Assuming the identifier is in the first column
        value = int(columns[2])  # Assuming the value is in the third column

        if identifier not in identifier_stats:
            identifier_stats[identifier] = {
                "sum": 0,
                "count": 0,
                "squared_diff": 0,
                "zeros": 0
            }

        identifier_stats[identifier]["sum"] += value
        identifier_stats[identifier]["count"] += 1
        identifier_stats[identifier]["squared_diff"] += value ** 2
        if value == 0:
            identifier_stats[identifier]["zeros"] += 1

# Prepare data for plotting
identifiers = list(identifier_stats.keys())
averages = np.array([stats["sum"] / stats["count"] for stats in identifier_stats.values()])
std_devs = np.array([math.sqrt((stats["squared_diff"] / stats["count"]) - (stats["sum"] / stats["count"]) ** 2) for stats in identifier_stats.values()])
zero_percentages = np.array([(stats["zeros"] / stats["count"]) * 100 for stats in identifier_stats.values()])

# Create custom colormap for the gradient based on zero percentages
#cmap = LinearSegmentedColormap.from_list('custom_cmap', [(0, 'green'), (1, 'red')])

# Normalize zero percentages to be in the [0, 1] range for colormap
#normalized_zero_percentages = zero_percentages / 100

# Create the scatter plot
plt.figure(figsize=(10, 6))
#scatter = plt.scatter(identifiers, averages, c=normalized_zero_percentages, cmap=cmap, s=100, alpha=1, edgecolors='black', zorder=3)
scatter = plt.scatter(identifiers, averages, c=zero_percentages, cmap='RdYlGn_r', s=100, alpha=1, edgecolors='black', zorder=3)
#Plot error bars (not recommended)
#plt.errorbar(identifiers, averages, yerr=std_devs, fmt='none', markersize=10, ecolor='black', elinewidth=1, capsize=4, zorder=1)
plt.colorbar(scatter, label='Percentage of missing coverage')

# Set labels and title
plt.xlabel('Chromosome')
plt.ylabel('Average coverage')
plt.title(f"Average coverage by chromosome: {file_base_name}")
plt.grid(axis='y', linestyle='--', alpha=0.7)

# Rotate x-axis labels for better readability
plt.xticks(rotation=45, ha='right')

# Print the initial list to STDOUT
print("Identifier\tAverage Coverage\tStandard Deviation\tPercentage of Zeros")
for identifier, stats in identifier_stats.items():
    average = stats["sum"] / stats["count"]
    variance = (stats["squared_diff"] / stats["count"]) - (average ** 2)
    std_dev = math.sqrt(variance) if variance >= 0 else 0
    zero_percentage = (stats["zeros"] / stats["count"]) * 100
    print(f"{identifier}\t{average:.2f}\t{std_dev:.2f}\t{zero_percentage:.2f}%")

# Show the plot
output_file_name = f"{file_base_name}_coverage_plot.png"
plt.savefig(output_file_name, dpi=600)
plt.close()
#plt.show()
print("Plot saved as:", output_file_name)

# Stop timer and print exit message
end_time = time.time()
elapsed_time = end_time - start_time
print("Script execution time:", elapsed_time, "seconds")