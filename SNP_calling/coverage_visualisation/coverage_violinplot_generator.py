# coverage_violinplot_generator.py v0.2

# Generate an average coverage per chromosome plot (filtered) by using the samtools depth output.
# Usage: python3 coverage_violinplot_generator.py <INPUT>

import os
import argparse
import matplotlib.pyplot as plt
import time
import numpy as np

def main():
    # Set up command-line argument parser
    parser = argparse.ArgumentParser(description="Generate violin plots of coverage by identifier")
    parser.add_argument("input_file", type=str, help="Path to the input file")
    args = parser.parse_args()

    # Extract file information
    input_file_path = args.input_file
    file_base_name, file_extension = os.path.splitext(os.path.basename(input_file_path))
    
    # Check if the input file exists
    if not os.path.exists(input_file_path):
        print("Error: Input file does not exist.")
        return

    # Read data from the input file and organize it by identifier
    identifier_data = {}
    with open(input_file_path, "r") as file:
        # Extract header information
        header = file.readline().strip().split("\t")[1:]

        # Process data lines
        for line in file:
            columns = line.strip().split("\t")
            identifier = columns[0]
            value = int(columns[2])

            if identifier not in identifier_data:
                identifier_data[identifier] = []

            identifier_data[identifier].append(value)

    # Calculate IQR and define threshold for outlier removal
    all_values = [value for values in identifier_data.values() for value in values]
    Q1 = np.percentile(all_values, 25)
    Q3 = np.percentile(all_values, 75)
    IQR = Q3 - Q1
    outlier_threshold = 1.5 * IQR

    # Remove outliers beyond the threshold
    identifier_data_filtered = {}
    for identifier, values in identifier_data.items():
        filtered_values = [value for value in values if (Q1 - outlier_threshold) <= value <= (Q3 + outlier_threshold)]
        identifier_data_filtered[identifier] = filtered_values

    # Prepare data for violin plot
    data_to_plot = [values for values in identifier_data_filtered.values()]

    # Create and customize the violin plot
    plt.figure(figsize=(12, 6))
    violinplot = plt.violinplot(data_to_plot, showmedians=True)

    # Calculate and display the overall average of the filtered data
    filtered_all_values = [value for values in identifier_data_filtered.values() for value in values]
    filtered_overall_average = sum(filtered_all_values) / len(filtered_all_values)
    plt.axhline(y=filtered_overall_average, color='orange', linestyle='--', label="Overall Average (filtered)")

    # Set plot labels, title, and formatting
    plt.xlabel("Identifier")
    plt.ylabel("Coverage")
    plt.title(f"Average Coverage by Identifier: {file_base_name}")
    plt.xticks(range(1, len(identifier_data_filtered) + 1), identifier_data_filtered.keys(), rotation=45, ha="right")
    plt.grid(axis='y', linestyle='--', alpha=0.7)
    plt.legend(loc='upper right')
    plt.tight_layout()

    # Save the generated violin plot as a PNG image file
    output_file_name = f"{file_base_name}_filtered_violinplot.png"
    plt.savefig(output_file_name, dpi=600)  # Save with higher DPI for better quality
    plt.close()

    print("Filtered violin plots saved as:", output_file_name)

if __name__ == "__main__":
    # Measure script execution time
    start_time = time.time()
    main()
    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Script execution time:", elapsed_time, "seconds")
