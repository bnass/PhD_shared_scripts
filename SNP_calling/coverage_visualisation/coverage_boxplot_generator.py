# coverage_boxplot_generator.py v0.2

# Generate an average coverage per chromosome plot (filtered) by using the samtools depth output.
# Usage: python3 coverage_boxplot_generator.py <INPUT>

import os
import argparse
import matplotlib.pyplot as plt
import time

def main():
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(description="Generate boxplots of coverage by identifier")
    parser.add_argument("input_file", type=str, help="Path to the input file")
    args = parser.parse_args()

    # Extract input file path and base name
    input_file_path = args.input_file
    file_base_name, file_extension = os.path.splitext(os.path.basename(input_file_path))
    
    # Check if the input file exists
    if not os.path.exists(input_file_path):
        print("Error: Input file does not exist.")
        return

    # Initialize a dictionary to store data by identifier
    identifier_data = {}

    # Read and process the input file
    with open(input_file_path, "r") as file:
        # Extract header information
        header = file.readline().strip().split("\t")[1:]

        # Process each line in the file
        for line in file:
            columns = line.strip().split("\t")
            identifier = columns[0]
            value = int(columns[2])

            # Add data to the dictionary
            if identifier not in identifier_data:
                identifier_data[identifier] = []

            identifier_data[identifier].append(value)

    # Prepare data for boxplot
    data_to_plot = [values for identifier, values in identifier_data.items()]

    # Set up the boxplot with customized styles
    plt.figure(figsize=(12, 6))
    boxplot = plt.boxplot(data_to_plot,
                          showfliers=False,
                          patch_artist=True,
                          boxprops={'linewidth': 1, 'facecolor': 'cornflowerblue', 'edgecolor': 'black'},
                          whiskerprops={'linewidth': 1, 'color': 'black'},
                          medianprops={'linewidth': 1, 'color': 'black'},
                          capprops={'linewidth': 1, 'color': 'black'})

    # Calculate overall average of the entire input file
    all_values = [value for values in identifier_data.values() for value in values]
    overall_average = sum(all_values) / len(all_values)
    
    # Add a line for the overall average
    plt.axhline(y=overall_average, color='orange', linestyle='--', label="Overall Average")

    # Set labels and title
    plt.xlabel("Identifier")
    plt.ylabel("Coverage")
    plt.title(f"Average Coverage by Identifier: {file_base_name}")
    plt.xticks(range(1, len(identifier_data) + 1), identifier_data.keys(), rotation=45, ha="right")
    
    # Add grid and legend
    plt.grid(axis='y', linestyle='--', alpha=0.7)
    plt.legend(loc='upper right')

    # Adjust layout for better visualization
    plt.tight_layout()

    # Save the boxplot as a PNG image file
    output_file_name = f"{file_base_name}_boxplot.png"
    plt.savefig(output_file_name, dpi=600)  # Save with higher DPI for better quality
    plt.close()

    # Print a message indicating the filename of the saved boxplot
    print("Boxplots saved as:", output_file_name)

if __name__ == "__main__":
    # Measure script execution time
    start_time = time.time()
    main()
    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Script execution time:", elapsed_time, "seconds")
