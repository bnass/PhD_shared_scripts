#!/bin/bash
# Version 1.2b

# not tested yet, added index and dictionary support
# Chromosome list is hardcoded, has to be manually edited

# Check if arguments are provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <bam_directory> [<reference_fasta_file>]"
    exit 1
fi

# Specify the directory containing your BAM files
bam_dir="$1"

# Check if the specified directory exists
if [ ! -d "$bam_dir" ]; then
    echo "Directory not found: $bam_dir"
    exit 1
fi

# Use the provided reference FASTA file or look in the "reference" directory
if [ -n "$2" ]; then
    ref_fasta="$2"
else
    ref_fasta=$(find "${bam_dir}/reference" -maxdepth 1 -type f -name "*.fasta" | head -n 1)
fi

# Check if the reference FASTA file exists
if [ ! -e "$ref_fasta" ]; then
    echo "Reference FASTA file not found: $ref_fasta"
    exit 1
else
	echo "Reference FASTA file found: $ref_fasta"
fi

# Check if the reference FASTA file has been indexed; if not, index it
ref_index="${ref_fasta}".fai
if [ ! -e "$ref_index" ]; then
    echo "Indexing reference FASTA file. Please wait..."
    module load samtools
    samtools faidx "$ref_fasta"
    if [ $? -eq 0 ]; then
        echo "Reference FASTA file has been indexed."
    else
        echo "Error indexing reference FASTA file."
        exit 1
    fi
fi

# Check if there is a dictionary file of the reference; if not, create it
ref_dict="${ref_fasta%.fasta}.dict"
if [ ! -e "$ref_dict" ]; then
    echo "Creating dictionary file. Please wait..."
    module load jdk
	module load gatk
    gatk CreateSequenceDictionary -R "$ref_fasta"
    if [ $? -eq 0 ]; then
        echo "Dictionary of reference FASTA file has been created."
    else
        echo "Error creating dictionary file."
        exit 1
    fi
fi

cd $bam_dir

# Loop through each .bam file and submit a job for each chromosome
for bam_file in "$bam_dir"/*.md.bam; do
    # Sanitize the job name to replace problematic characters
    job_name="$(basename "$bam_file" | tr -cs '[:alnum:]_' '_')"
	
	# This is a custom list for a specific genome, edit to fit your data
	# If you don't know how, use: $samtools view -H your_file.bam | grep "@SQ"
    for chr in Cs1C Cs2C Cs3C Cs4C Cs5C Cs6C Cs7C Cs8C Cs9C Cs1D Cs2D Cs3D Cs4D Cs5D Cs6D Cs7D Cs8D Cs9D; do
        # Create a temporary directory for this chromosome
        tmp_dir="${job_name}${chr}_tmp"
        mkdir -p "$tmp_dir"

        qsub <<EOT
#!/bin/bash
#PBS -N variant_calling_${job_name}${chr}
#PBS -l select=1:ncpus=8:mem=16gb:scratch_local=10gb
#PBS -l walltime=72:0:0
#PBS -j oe
#PBS -m abe

# Test if scratch directory is set
# If scratch directory is not set, issue an error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# Load any necessary modules
# module load your_module
echo "Loading required modules..."
module add jdk
module add gatk

echo "Switching path to $bam_dir"
cd $bam_dir

echo "Starting HaplotypeCaller..."
gatk --java-options "-Xmx16g" HaplotypeCaller \
--input $bam_file \
--output ${bam_file%.md.bam}.$chr.gvcf.gz \
--reference $ref_fasta \
--emit-ref-confidence GVCF \
--native-pair-hmm-threads 8 \
--L $chr \
--tmp-dir $tmp_dir

# Remove the temporary directory
echo "Removing $tmp_dir ..."
rm -d $tmp_dir

echo "Done! Exiting..."
EOT
        echo "Job for ${bam_file} (chromosome $chr) has been submitted!"
    done
done
