# ambyeguity.py v1.1

# Modify a PHYLIP file by replacing non-nucleotide characters with "N".

# usage: python3 ambyeguity.py <INPUT PHYLIP> <OUTPUT PHYLIP>

import argparse

def replace_non_nucleotide(sequence):
    """
    Replaces non-nucleotide characters with 'N' in a sequence.
    """
    nucleotides = {'A', 'T', 'G', 'C', 'N'}
    return ''.join('N' if char.upper() not in nucleotides else char for char in sequence)


def modify_phylip_file(input_file, output_file):
    """
    Modifies a PHYLIP file by replacing non-nucleotide characters with 'N' in the sequences.
    """
    sequences = []
    with open(input_file, 'r') as f:
        lines = f.readlines()
        num_sequences = int(lines[0].strip().split()[0])
        phylip_header = lines[0].strip()
        for line in lines[1:]:
            line = line.strip()
            if line:
                sequence_id, sequence = line.split(None, 1)
                modified_sequence = replace_non_nucleotide(sequence)
                sequences.append((sequence_id, modified_sequence))
    
    with open(output_file, 'w') as f:
        f.write(f"{phylip_header}\n")
        for sequence_id, sequence in sequences:
            f.write(f"{sequence_id}\t{sequence}\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Modify a PHYLIP file by replacing non-nucleotide characters with "N"')
    parser.add_argument('input_file', help='Input PHYLIP file')
    parser.add_argument('output_file', help='Output PHYLIP file')
    args = parser.parse_args()

    try:
        modify_phylip_file(args.input_file, args.output_file)
        print('PHYLIP file modification completed successfully.')
    except FileNotFoundError:
        print('Error: Input file not found.')
    except Exception as e:
        print(f'An error occurred: {str(e)}')
