#!/bin/bash
### RAxML_preparation_pipeline v1.3.1 ####

# This script performs various data preprocessing steps, including VCF filtering, format conversion,
# removal of invariant sites, and preparation of files required for the ascertainment bias correction
# in RAxML analysis (instructions at the end).
# usage: bash ./RAxML_preparation_pipeline.sh <input_vcf> <length_of_loci>

# Please place all required scripts (and the VCF) in the same directory as the pipeline.
# If the vcf file was filtered using vcftools, see the vcf_filtering_vcftools.sh script for details.

# Loci length is calculated by total no. of sites from 'population.log' - from line 'Kept 80583 loci,
# composed of 69885517 sites; 1045927 of those sites were filtered, 1209192 variant sites remained.'
# 69885517/80583=867.2 (float) -> 867 (int)
# Calculation might by included in a later version...

input_file="$1"  # The first command-line argument should be the input file path
loci_len="$2"  # The second command-line argument should be the loci length

# Check if the input file argument or loci length argument is missing
if [ -z "$input_file" ] || [ -z "$loci_len" ]; then
  echo "Error: Please provide the input file path and the loci lenght as command-line arguments."
  echo "Usage: ./RAxML_preparation_pipeline.sh <input_vcf> <length_of_loci>"
  exit 1
fi

# Check if the input file format is correct
input_file_name="${input_file%.*}"  # Remove the first extension

if [[ "$input_file" == *.gz ]]; then
    input_file_name="${input_file_name%.*}"  # Remove the second extension
fi

if [[ "$input_file" != *.vcf && "$input_file" != *.vcf.gz ]]; then
    echo "Error: Invalid input format. Supported formats are .vcf and .vcf.gz"
    exit 1
fi

echo "Processed filename: $input_file_name"

# Convert loci_len to string if it's a float value
if [ "$(echo "$loci_len" | grep -c '\.')" -eq 1 ]; then
  loci_len=$(printf "%.0f" "$loci_len")
fi

# Function to check the exit status and print error message if so
check_exit_status() {
  exit_status=$1
  error_message=$2

  if [ $exit_status -ne 0 ]; then
    echo "Error: $error_message"
    exit $exit_status
  fi
}

# vcf2phylip.py: converts vcf to nexus, fasta, phylip 
# https://github.com/edgardomortiz/vcf2phylip
python3 vcf2phylip.py -i "$input_file" -n -f
check_exit_status $? "An error occurred in vcf2phylip.py"

# Removal of the IUPAC Ambiguity Codes for Nucleotide Degeneracy via ambyeguity.py
python3 ambyeguity.py "$input_file_name.min4.phy" "$input_file_name.noamb.phy"
check_exit_status $? "An error occurred in ambyeguity.py"

# Invariant sites removed using Dijana's approach, using raxml_ascbias
# https://github.com/btmartin721/raxml_ascbias
python3 ascbias.py -p "$input_file_name.noamb.phy" -o "$input_file_name.noamb.noinvar.phy"
check_exit_status $? "An error occurred in ascbias.py"

# Prepare file 'part' file/'p1.txt'-> no. of variable sites from first
# line of the phy file/ total no. of sites from 'population.log'.
python3 raxml_stat_calc.py "$input_file_name.noamb.noinvar.phy" "$input_file" "$loci_len"
check_exit_status $? "An error occurred in raxml_stat_calc.py"

# Print the finishing message
echo "RAxML_preparation_pipeline finished successfully!"

# Additional RAxML parameters (for testing, full runs on metacentrum):

# Initial tree calculation:
# raxmlHPC-PTHREADS -m ASC_GTRCAT -V --K80 --asc-corr=felsenstein -n besttree -s <out_phylip> -p 12345 -# 20 -q part -T 24

# Bootstrapping using the autoFC stopping criterion:
# raxmlHPC-PTHREADS -m ASC_GTRCAT -V --K80 --asc-corr=felsenstein -n bootstrap -s <out_phylip> -p 12345 -b 54321 -# autoFC -q part -T 24

# Summarize RAXML bootstrap
# raxmlHPC-PTHREADS-SSE3 -m GTRCAT -p 12345 -f b -t RAxML_bestTree.besttree -z RAxML_bootstrap.bootstrap -n bestree_w_bootstrap