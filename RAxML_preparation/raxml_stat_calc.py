# raxml_stat_calc.py v1.2

# Prepare file 'part' and 'p1.txt': no. of variable sites from first
# line of the phy file/ total no. of sites from 'population.log'.

# Usage: python3 raxml_stat_calc.py <PHYLIP> <VCF> <Loci Lenght>

# Change PHY, VCF and length_loci accordingly.

import argparse
import subprocess
import gzip
import os

def get_file_handle(file_path):
    _, file_extension = os.path.splitext(file_path)
    if file_extension == '.gz':
        return gzip.open(file_path, 'rt')
    else:
        return open(file_path, 'r')

# Parse command-line arguments
parser = argparse.ArgumentParser(description='Process PHY and VCF files.')
parser.add_argument('phy_file', help='Path to the PHY file')
parser.add_argument('vcf_file', help='Path to the VCF file')
parser.add_argument('length_loci', type=int, help='Length of each locus') #for my example: 867 -> 6988551/80583=867.2
args = parser.parse_args()

# Calculate phy_length
with get_file_handle(args.phy_file) as phy_handle:
    first_line = phy_handle.readline()
    output = first_line.split()
    phy_length = output[1]

# Create 'part' file
with open('part', 'w') as part_file:
    part_file.write(f"[asc~p1.txt], ASC_DNA, p1=1-{phy_length}\n")

# Calculate length_align and save it to 'p1.txt'
with get_file_handle(args.vcf_file) as vcf_handle:
    lines = vcf_handle.readlines()[15:]  # Skip the first 15 lines
    no_of_loci = len(lines)
    length_align = no_of_loci * args.length_loci

with open('p1.txt', 'w') as p1_file:
    p1_file.write(str(length_align) + '\n')
